#! /usr/bin/env python3

# -*- coding: utf-8 -*-

"""
Helper file.
Commandline wizards to setup config files for apps.
"""

from sys import platform
from helpers.colors import Colors
from helpers.file import Location, FileInfo, FileEdit
from helpers.io import IO
from helpers.inp import Inp
from helpers.encrypt import Encryption

__author__ = "Troy Franks"
__version__ = "2023-05-13"

loc = Location()
fi = FileInfo()
fe = FileEdit()
io = IO()
inp = Inp()
en = Encryption()
clr = Colors()


def get_ip():
    # os = platform
    if platform.startswith("linux"):
        ipaddr = fi.command_var("hostname -I")
    elif platform.startswith("windows"):
        ipaddr = fi.command_var("ipconfig /release")
    elif platform.startswith("darwin"):
        ipaddr = fi.command_var("ifconfig |grep inet")
    return ipaddr


class WizardHome:
    home = loc.home_dir()
    no_config = "We could not find any configuration folder\n"
    intro_desc_1 = (
        "This wizard will ask some questions to "
        "setup the\nconfiguration needed for "
        "the script to function\n"
    )
    intro_desc_2 = "This configuration wizard will only run once.\n"
    intro_desc_3 = (
        "The first 2 questions are going to be about your email\n"
        "and password you are using to send.\n"
        "This login information will be stored on your local computer\n"
        "encrypted seperate from the rest of the configuration.\n"
    )

    intro_desc_4 = "This is not viewable by browsing the filesystem\n"
    statement = "\nThis completes the wizard"
    message_1 = (
        f"\nThe configuration file has been written to disk"
        f"\nIf you want to change the settings you can edit "
    )
    message_2 = (
        f"\nThis wizard "
        f"won't run any more, So the script can "
        f"now be run automatically\n"
    )
    message_3 = f"You can stop the script by typing Ctrl + C\n"

    def __init__(self):
        pass

    def print_intro_message(self):
        print(
            (
                f"{clr.yellow_bold(self.no_config)}\n"
                f"{clr.green_bold(self.intro_desc_2)}"
                f"{self.intro_desc_1}"
            )
        )

    def print_final_message(
        self,
        conf_dir: str,
        conf_file: str,
    ):
        conf_dir_str = f"{self.home}/{conf_dir}/{conf_file}"
        print(
            (
                f"{clr.yellow_bold(self.statement)}"
                f"{self.message_1}"
                f"{clr.cyan_bold(conf_dir_str)}"
                f"{clr.green_bold(self.message_2)}"
                f"{clr.cyan_bold(self.message_3)}"
            )
        )

    def check_conf_exists(
        self,
        conf_dir: str,
        conf_file: str,
    ):
        dir_exists = fi.check_dir(conf_dir)
        file_exists = fi.check_file_dir(f"{conf_dir}/{conf_file}")
        if file_exists:
            print(
                (
                    f"\n"
                    f"Configuration file "
                    f"{clr.purple_bold(f'{self.home}/{conf_dir}/{conf_file}')}"
                    f" Already exists.\nIf want to change a setting please edit\n"
                    f"the file directly."
                    f"\n"
                )
            )
            return file_exists

        elif dir_exists is False:
            fe.make_dir(conf_dir)

    def check_encrypt_files(
        self,
        conf_dir: str,
        key_file: str,
        tmp_crypt: str,
        crypt_file: str,
        email_status: bool = False,
    ):
        kf_exists = fi.check_file_dir(f"{conf_dir}/{key_file}")
        cred_exists = fi.check_file_dir(f"{conf_dir}/{crypt_file}")
        if email_status:
            if kf_exists is False and cred_exists is False:
                self.encrypt_setup(
                    conf_dir,
                    key_file,
                    tmp_crypt,
                    crypt_file,
                )

    def encrypt_setup(
        self,
        conf_dir: str,
        key_file: str,
        tmp_crypt: str,
        crypt_file: str,
    ):
        print((f"{self.intro_desc_3}" f"{clr.purple_bold(self.intro_desc_4)}"))

        en.gen_key(f"{conf_dir}/{key_file}")
        key = io.open_file(
            fname=f"{conf_dir}/{key_file}",
            fdest="home",
            mode="rb",
        )

        email = inp.input_single(
            in_message="\nEnter your email",
            in_type="email",
        )
        pas = inp.input_single(
            in_message="\nEnter your password",
            in_type="password",
        )
        lo = {email: pas}

        io.save_yaml(
            fname=f"{conf_dir}/{tmp_crypt}",
            fdest="home",
            content=lo,
        )

        en.encrypt(
            key=key,
            fname=f"{conf_dir}/{tmp_crypt}",
            e_fname=f"{conf_dir}/{crypt_file}",
            fdest="home",
        )
        fe.remove_file(f"{conf_dir}/{tmp_crypt}")

    def config_setup_weather(
        self,
        conf_dir: str,
        conf_file: str,
        key_file: str,
        crypt_file: str,
        tmp_crypt: str,
        email_status: bool,
    ):
        """
        conf_dir = Configuration dir. This would
        normally be in the .config/progName,
        This commandline wizard creates the
        program config dir and populates the
        setup configuration file. Sets up username
        and password for email notifications
        """
        if (
            self.check_conf_exists(
                conf_dir=conf_dir,
                conf_file=conf_file,
            )
            is True
        ):
            return

        self.print_intro_message()
        self.check_encrypt_files(
            conf_dir=conf_dir,
            key_file=key_file,
            tmp_crypt=tmp_crypt,
            crypt_file=crypt_file,
            email_status=email_status,
        )

        send_list = inp.input_list(
            subject="email address",
            description="to send to (example@gmail.com)",
            in_type="email",
        )

        ip = get_ip()
        ipadd = inp.input_single(
            in_message="Enter the IP address for the broker",
            in_type="ip4",
            default=ip,
        )

        zip_code = inp.input_single(
            in_message="Enter the 5 digit US Zip code for weather data",
            in_type="zip5",
        )

        units = inp.input_choice(
            in_message="Choose the unit of measure for weather collection",
            choices={
                1: "imperial",
                2: "metric",
            },
        )

        load = {
            "USE_API": True,
            "API": 2,
            "BROKER_ADDRESS": ipadd,
            "ZIP_CODE": zip_code,
            "UNITS": units,
            "DB_URI": "mongodb://localhost:27017",
            "DATABASE": "home",
            "SENDTO": send_list,
        }

        io.save_yaml(
            fname=f"{conf_dir}/{conf_file}",
            fdest="home",
            content=load,
        )
        self.print_final_message(conf_dir, conf_file)

    def config_setup_backup_notify(
        self,
        conf_dir: str,
        conf_file: str,
        key_file: str,
        crypt_file: str,
        tmp_crypt: str,
        email_status: bool,
    ):
        """
        conf_dir = Configuration dir. This would
        normally be in the .config/progName,
        This commandline wizard creates the
        program config dir and populates the
        setup configuration file. Sets up username
        and password for email notifications
        """
        if (
            self.check_conf_exists(
                conf_dir=conf_dir,
                conf_file=conf_file,
            )
            is True
        ):
            return
        self.print_intro_message()
        self.check_encrypt_files(
            conf_dir=conf_dir,
            key_file=key_file,
            tmp_crypt=tmp_crypt,
            crypt_file=crypt_file,
            email_status=email_status,
        )

        ntfy = ""
        send_list: list = []

        if email_status is False:
            ntfy = inp.input_single(
                in_message="Enter the ntfy topic you want to use",
                in_type="string",
            )
        elif email_status is True:
            send_list = inp.input_list(
                subject="email address",
                description="to send to (example@gmail.com)",
                in_type="email",
            )

        run = inp.input_single(
            in_message="Enter the time to run the script daily(Example: 05:00)",
            in_type="time24",
        )
        numb_lines = inp.input_single(
            in_message="Enter the number of lines from the end of log file to send",
            in_type="number",
            max_number=400,
            default="50",
        )

        log_list = inp.input_list(
            subject="log file",
            description=(
                f"to check relative to your home"
                f"dir (Example: Logs/net_backup.log)"
            ),
            in_type="file",
        )
        load = {
            "runtime": run,
            "lines": int(numb_lines),
            "sendto": send_list,
            "logs": log_list,
            "topic": ntfy,
        }
        io.save_yaml(
            fname=f"{conf_dir}/{conf_file}",
            fdest="home",
            content=load,
        )

        self.print_final_message(conf_dir, conf_file)


if __name__ == "__main__":
    app = WizardHome()
